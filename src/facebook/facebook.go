package main 

import (
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "encoding/xml"
    "encoding/json"
    "strings"
    "sort"
    "os"
    "time"
)

const (
    // See http://golang.org/pkg/time/#Parse
    RFC1123 = "Mon, 02 Jan 2006 15:04:05 MST"
)

type Configuration struct {
    Feeds    []string
    DaysOld  float64
}

type RSS struct {
    XMLName xml.Name `xml:"rss"`
    Items Items `xml:"channel"`
}
type Items struct {
    XMLName xml.Name `xml:"channel"`
    ItemList []Item `xml:"item"`
}
type Item struct {
    Title string `xml:"Title"`
    Link string `xml:"link"`
    Description string `xml:"description"`
    Pubdate string `xml:"pubDate"`
}

type Links struct {
    XMLName xml.Name `xml:"links_getStats_response"`
    ItemList []Linkstat `xml:"link_stat"`
}

type Linkstat struct {
    Url string `xml:"url"`
    Total_count int `xml:"total_count"`
}

type LinkCount struct {
	UrlList []FBCount
}

type FBCount struct {
	Url string
	Count int
}

type ByCount []Linkstat

func (a ByCount) Len() int { 
	return len(a) 
}

func (a ByCount) Swap(i, j int)	{ 
	a[i], a[j] = a[j], a[i] 
}

func (a ByCount) Less(i, j int) bool { 
	return a[i].Total_count > a[j].Total_count 
}

func (box *Links) GetFeed(g string, daysLength float64) []Linkstat {
	res, err := http.Get(g)
    if err != nil {
        log.Fatal(err)
    }
    asText, err := ioutil.ReadAll(res.Body)
    if err != nil {
        log.Fatal(err)
    }

    var i RSS
    err = xml.Unmarshal([]byte(asText), &i)
    if err != nil {
        log.Fatal(err)  
    }
	
	var urlstring string
    for _, item := range i.Items.ItemList {
    	var itemLink string
    	var urlLink []string
    	then, err := time.Parse(RFC1123, item.Pubdate)
    	if err != nil {
     	   log.Fatal(err)
		}
		duration := time.Since(then)
		daysOld := duration.Hours()/24
		//fmt.Println(duration.Hours()/24)
		if daysOld < daysLength {
	        itemLink = strings.TrimSpace(item.Link)
	        urlLink = strings.SplitAfter(itemLink, "?")
	    	urlstring += strings.Replace(urlLink[0],"?","",-1)+","
    	}
    }
    
    res.Body.Close()
    fmt.Println(urlstring)
    if urlstring != "" {
		resfeed, err := http.Get("https://api.facebook.com/method/links.getStats?urls="+urlstring)
	        
	    if err != nil {
	        log.Fatal(err)
		}
		
		file, err := ioutil.ReadAll(resfeed.Body)
		if err != nil {
		log.Fatal(err)
		}
		
		var t Links
		err = xml.Unmarshal([]byte(file), &t)
		if err != nil {
			log.Fatal(err)  
		}
		
		for _, link := range t.ItemList {
			box.ItemList = append(box.ItemList, link)
		}
		
		resfeed.Body.Close()
		return box.ItemList
	}
	
	return box.ItemList
}

func main() {
		var fbCount Links
		var counter LinkCount
		var conf Configuration
		
		file, _ := os.Open("conf.json")
		decoder := json.NewDecoder(file)
		err := decoder.Decode(&conf)
		if err != nil {
		  fmt.Println("error:", err)
		}
		fmt.Println(conf.Feeds)
		fmt.Println(conf.DaysOld)
		file.Close()
		
		for _, feeds := range conf.Feeds {
			fbCount.GetFeed(feeds, conf.DaysOld)
		}
		
		fmt.Println(len(fbCount.ItemList))
		
		//sort descending
		sort.Sort(ByCount(fbCount.ItemList))
		
		for _, link := range fbCount.ItemList {
			t := FBCount{link.Url,link.Total_count}
			counter.UrlList = append(counter.UrlList, t)
		}
		
		b, err := json.Marshal(counter)
		
		if err != nil {
			log.Fatal(err)  
		}
		 
		 jsonFile, err := os.Create("./data.json")
		 
		 if err != nil {
            log.Fatal(err)  
         }
		 
		 defer jsonFile.Close()

         jsonFile.Write(b)
         jsonFile.Close()

}